# Angelica Salas Tovar
# Lab 3 python
# Sequence of floating point numbers 

def main(): 
#declaring numbers 
numbers = [645.32, 37.40, 76.30, 5.40, -34.23, 1.11, -34.94, 23.37, 635.46, -876.22, 467.73, 62.26]

#declaring people
people = [
    {'name': 'Hal', 'age': 20},
    {'name': 'Susann', 'age': 31},
    {'name': 'Dwight', 'age': 19},
    {'name': 'Kassandra', 'age': 21},
    {'name': 'Lawrence', 'age': 25},
    {'name': 'Cindy', 'age':22},
    {'name': 'Cory', 'age': 27},
    {'name': 'Mac', 'age': 19},
    {'name': 'Romana', 'age': 27},
    {'name': 'Doretha', 'age': 32},
    {'name': 'Danna', 'age': 20},
    {'name': 'Zara', 'age': 23},
    {'name': 'Rosalyn', 'age': 26},
    {'name': 'Risa', 'age': 24},
    {'name': 'Benny', 'age': 28},
    {'name': 'Juan', 'age': 33},
    {'name': 'Natalie', 'age': 25}]
    
#sort number
numbers.sort()

#Sorting with lambda function and returns a tuple

#sort by name
sorted(people, key=lambda x: (x['name'], x['age']))

#sort by age
sorted(people, key=lambda y: (y['age'], y['name']))