-- Angelica Salas Tovar
-- Lab 3 Haskell

module QuickSort where
    qsort :: Ord a => [a] -> [a]
    qsort []     = []
    qsort (x:xs) = qsort smaller ++ [x] ++ qsort larger
        where smaller  = filter (<= x) xs
              larger = filter (> x) xs