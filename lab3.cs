// Angelica Salas Tovar
// Lab 4 C#
// Sequence of floating point numbers
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Generics
{
	public class Person{
		public string name;
		public int age;

		public Person(string name, int age){
			this.name = name;
			this.age = age;
		}
	}
	public static void Main(string[] args)
		{
			
			List<double> numbers = new List<double> {645.32, 37.40, 76.30, 5.40, -34.23, 1.11, -34.94, 23.37, 635.46, -876.22, 467.73, 62.26};
			List<Person> person = new List<Person>();
			person.Add(new Person("Hal", 20));
			person.Add(new Person("Susann", 31));
			person.Add(new Person("Dwight", 19));
			person.Add(new Person("Kassandra", 21));
			person.Add(new Person("Lawrence", 25));
			person.Add(new Person("Cindy", 22));
			person.Add(new Person("Cory", 27));
			person.Add(new Person("Mac", 19));
			person.Add(new Person("Romana", 27));
			person.Add(new Person("Doretha", 32));
			person.Add(new Person("Danna", 20));
			person.Add(new Person("Zara", 23));
			person.Add(new Person("Rosalyn", 26));
			person.Add(new Person("Risa", 24));
			person.Add(new Person("Benny", 28));
			person.Add(new Person("Juan", 33));
			person.Add(new Person("Natalie", 25));


			numbers.Sort();
		
			person.OrderBy(p=>p.age);
			person.OrderBy(p=>p.name);
		}
}