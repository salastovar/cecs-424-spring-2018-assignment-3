module Main where

    import QuickSort -- import my quick sort function

    main = do
        let numbers = [645.32, 37.40, 76.30, 5.40, -34.23, 1.11, -34.94, 23.37, 635.46, -876.22, 467.73, 62.26]

        let people = [Hal, 20; Susann, 31; Dwight 19; Kassandra, 21; Lawrence, 25; Cindy, 22; Cory, 27;
        Mac, 19; Romana, 27; Doretha, 32; Danna, 20; Zara, 23; Rosalyn, 26; Risa, 24; Benny,
        28; Juan, 33; Natalie, 25]
      
        putStr "Quick Sort Outputs = "
        putStr "Numbers = "
        print (qsort numbers)
        putStr "People = "
        print (qsort people)